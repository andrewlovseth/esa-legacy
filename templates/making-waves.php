<?php

/*
    Template Name: Making Waves
*/

get_header(); ?>

    <div class="making-waves-header">
        <h1><?php the_field('headline'); ?></h1>
        <h2><?php the_field('sub_headline'); ?></h2>
        <p><?php the_field('copy'); ?></p>
    </div>

    <div class="making-waves-body">
        <div class="sign-up-form">
            <?php the_field('form'); ?>
        </div>
    </div>

<?php get_footer(); ?>