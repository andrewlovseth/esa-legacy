<?php

/*
    Template Name: Survey
*/

get_header(); ?>

    <main class="esa-content">
        <div class="grid-wrapper-left-two-thirds">
            <div class="left-two-thirds">

                <?php get_template_part('templates/survey/page-header');  ?>

                <?php get_template_part('templates/survey/options');  ?>

                <?php get_template_part('templates/survey/survey');  ?>
                
            </div>
        </div>
    </main>    

<?php get_footer(); ?>