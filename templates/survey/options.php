<div class="options">
    <div class="headline">
        <h3><?php the_field('options_headline'); ?></h3>
    </div>
    
    <?php if(have_rows('options')): while(have_rows('options')): the_row(); ?>
        <?php 
            $link = get_sub_field('link');
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <div class="option">
            <div class="logo">
                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                    <img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </div>

            <div class="info">
                <div class="name">
                    <h3><?php the_sub_field('name'); ?></h3>
                </div>

                <div class="description">
                    <p><?php the_sub_field('description'); ?></p>
                </div>

                <div class="cta">
                    <a class="link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">Learn more</a>
                </div>
            
            </div>
            
        </div>

    <?php endwhile; endif; ?>        
</div>