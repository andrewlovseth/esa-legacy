<?php

    $survey = get_field('survey');
    $headline = $survey['headline'];
    $embed = $survey['embed'];

?>

<div class="survey">
    <div class="headline">
        <h3><?php echo $headline; ?></h3>
    </div>

    <div class="embed">
        <?php echo $embed; ?>
    </div>
</div>