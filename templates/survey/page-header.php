<?php

    $page_header = get_field('page_header');
    $headline = $page_header['page_title'];
    $summary = $page_header['summary'];
    $photo = $page_header['photo'];

?>

<div class="page-header">
    <div class="page-title">
        <h1><?php echo $headline ?></h1>
    </div>

    <div class="summary">
        <h4><?php echo $summary; ?></h4>
    </div>

    <?php $image = get_field(''); if( $photo ): ?>
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
            <?php if($photo['caption']): ?>
                <div class="caption">
                    <span><?php echo $photo['caption']; ?></span>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

</div>

