<?php
/**
 * The header for Astra Theme. 
 *
 * This is the template that displays all of the <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252"> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @since 1.0.0
 */
?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-24892454-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-24892454-1');
</script>
	
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script></li>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">
	
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700|Bitter:700&display=swap" rel="stylesheet"> 
<!-- link href="https://fonts.googleapis.com/css?family=Bitter:700|Raleway:400,700,800" rel="stylesheet" -->
	
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">		
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
<?php astra_head_bottom(); ?>
<?php wp_head(); ?>
<style>
@use autoprefixer {
  remove: false;
  grid: true;
  browsers: "> 1%, firefox 32";
}
.blue-callout {
  float: right;
  max-width: 430px;
  background-color: #D5EDEF;
  margin: 20px 0 20px 20px;
  padding: 30px;
}
.main-header-menu, .main-header-menu a, .ast-header-custom-item, .ast-header-custom-item a, .ast-masthead-custom-menu-items, .ast-masthead-custom-menu-items a {
	color: #6d6e71;
}
.wpforms-field-hp {
	display: none; visibility: hidden;
}
#wpforms-13694-field_1 {
    width: calc(100% - 65px) !important;
	max-width: calc(100% - 65px) !important;
    height: 50px !important;
}
main {
  display: grid;
}
.admin-button {
	color: #fff !important;
	background-color: transparent !important;
	padding: 0 !important;
	border: none !important;
	font-size: 14px;
}
code {
    padding: 2px 4px;
    font-size: 90%;
    color: #c7254e;
    background-color: #f9f2f4;
    border-radius: 4px;
    font-family: Raleway !important;
	display: block !important;
	font-size: 18px !important;
	line-height: 27px !important;
}
.entry-content p {
    margin-bottom: 1em !important;
}
/*  REVERSE COLUMNS */
.color:nth-child(odd) {
    background-color: white;
}
.color:nth-child(even) {
    background-color: white;
}
.reverse {
    display: grid;
    grid-template-columns: 2fr min-content;
    grid-auto-flow: dense;
	padding: 70px 0;
}
.eo-content {
    grid-column: 1 / 2;
    text-align: center;
}
.eo-content:nth-child(2) {
    grid-column: 2 / 3;
}
.reverse > .eo-content {
    grid-column: 2 / 3;
}
.reverse > .eo-content:nth-child(2) {
    grid-column: 1 / 2;
}
@media(max-width: 767px){
    .eo-content {
        grid-column: 1 / 3;
    }
    .eo-content:nth-child(2) {
        grid-column: 1 / 3;
    }
    .reverse > .eo-content {
        grid-column: 1 / 3;
    }
    .reverse > .eo-content:nth-child(2) {
        grid-column: 1 / 3;
    }
}	
/* END OF REVERSE COLUMNS */
.search .entry-title a {
	font-weight: 700;
	font-size: 36px;
	color: #000 !important;
	line-height: 1;
}
.ast-header-break-point .footer-sml-layout-2 .ast-small-footer-section-2 {
	margin: 0 !important;
}
.wpt-form-description {
	font-size: 0.9em;
	color: red !important;
	margin-bottom: 7px;
	font-weight: 600 !important;
}	
.footer-adv-widget-1 {
	margin-right: -20px !important;
	width: 270px;
}
.footer-adv-widget-2 
{
	margin-right: 40px !important;
}
.footer-adv-widget-3 {
	width: 280px !important;
}
.footer-adv-widget-4 
{
	width: 260px !important;
}
#adminmenu div.separator {
	height: 2px !important;
	margin: 5px 0 !important;
	background-color: lightblue !important;
}
.about-title {
	font-family: Raleway;
	font-size: 48px;
	font-weight: 700;
	text-align: left;
	line-height: 1;
	padding-bottom: 30px;
}
.about-sub-title {
	font-family: Raleway;
	font-size: 36px;
	font-weight: 700;
	text-align: left;
	line-height: 1;
	padding-bottom: 30px;
}
.about-small-title {
	font-family: Raleway;
	font-size: 18px;
	font-weight: 700;
	text-align: left;
	line-height: 1.5;
}
.about-small-title-center {
	text-align: center; 
	font-weight: bold; 
	padding: 25px 0;
}
.about-title-center {
	font-family: Raleway;
	font-size: 48px;
	font-weight: 800;
	text-align: center;
	padding: 0 0 30px 0;
}
.about-text {
	padding: 20px 0 0 0;
}
.centered-highlight {
	text-align: center;
	max-width: 900px;
	margin: 0 auto;
	padding-bottom: 50px;
}
.grey-float-right {
	background-color: #EDEDED;
	float: right;
	right: 0;
	margin: 30px 0;
	/* width: 100%; */
}
.grey-float-content {
	max-width: 770px;
	float: left;
	padding: 25px;
	background-color: #ededed;
}
.join-us-title {
	font-family: Raleway;
	font-size: 48px;
	font-weight: 800;
	text-align: left;
	line-height: 1;
	padding-bottom: 30px;
}
.join-us-centered-title {
	font-family: Raleway;
	font-size: 48px;
	font-weight: 800;
	text-align: center;
}
#letters {
	display: none;
	visibility: hidden;
}
.inner-slider {max-width: 480px;}
.az-slider {
    max-width: 1180px !important;
    margin: 85px 0 0 0 !important;
    column-width: 500px !important;
}
.az-letters ul.az-links li a {
	color: #fff;
	background-color: #a0cf67;
}
li.first.odd.has-posts, li.even-has-posts {
	color: #fff;
	background-color: #a0cf67;
}
.az-letters ul.az-links li, .a-z-listing-widget .az-letters ul.az-links li, .a-z-listing-widget.widget .az-letters ul.az-links li {
	list-style: none;
	width: 2em;
	height: 2em;
	box-sizing: border-box;
	margin: 0 0 0.6em;
	border: 2px solid #e5e5e5;
	background: #a0cf67;
	color: #ededed;
	display: flex;
	align-items: center;
	justify-content: center;
}
li.first.odd.has-posts, li.even-has-posts {
	color: #fff;
	background-color: #a0cf67;
}
.letter-section {
    margin-bottom: 2em;
    max-width: 500px;
}
.letter-section ul.columns.max-0-columns, .letter-section ul.columns.max-1-columns {
	column-count: 1;
	max-width: 500px;
}
.ast-single-post-order {
	display: none;
	visibility: hidden;
}
.right-line {
	z-index: 200;
	height: 131px;
	width: calc(((100vw - 1180px) / 2) + 5px);
	border-bottom: 1px solid #ADAFB2;
	right: 0;
	top: 0;
	float: right;
	position: absolute;
	background-color: transparent; /* lightskyblue; */
}
.project-category-container {
	height: auto;
	width: 49%;
	max-width: 560px;
	display: inline-block;
	float: left;
	padding: 10px 20px 10px 0;
}
.project-category-photo {
	z-index: 5;
	width:100%;
	height:auto;
	max-width:560px;
}
.white-title-bar {
	background-color: #fff;
	height: 40px;
	display: inline-block;
	position: relative;
	right: 0 !important;
	width: 430px;
	z-index: 10;
	margin: -70px 0 0 0;
	font-size: 24px;
	font-family: Raleway, sans-serif;
	float: right;
}
a.projects-title  {
	background-color: #fff;
	height: 45px;
	margin: 0 0 0 15px;
	text-decoration: none !important;
	font-size: 16px;
	line-height: 2.8;
	color: #000;
	font-weight: 700;
	text-transform: uppercase;
}
.projects-green-button {
	color: #ffffff;
	background-color: #A0CF67;
	border-color: #A0CF67;
	display: inline-block;
	margin: 0;
	font-weight: 700 !important;
	text-align: center;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	white-space: nowrap;
	padding: 0;
	line-height: 1.3;
	border-radius: 0;
	font-family: Raleway, sans-serif;
	font-size: 30px;
	height: 40px;
	width: 40px;
	float: right;
	text-decoration: none !important;
	z-index: 1;
}
select {
  /* for Firefox */
  -moz-appearance: none;
  /* for Chrome */
  -webkit-appearance: none;
}

/* For IE10 */
select::-ms-expand {
  display: none;
}
.dropdown:not(:first-child) {
  margin-top: 1em;
}
.dropdown > :not(:first-child) {
  margin-top: unset;
}
.dropdown__label {
  display: block;
}
.dropdown__element {
  display: inline-block;
  padding: 0.5em;
  vertical-align: middle;
  background: #fff;
  border-radius: 0;
}
.select-wrap::after {
  margin-left: -2em;
  font-size: 0.75em;
  pointer-events: none;
  content: '🞃';
  color: blue;
}
select {
    padding: 0 0 0 5px;
}
primary, .dropdown, .form-group {
    color: #ffffff;
    background-color: #689BC5 !important;
    border: none;
    font-family: Raleway, sans-serif;
    font-size: 1.5rem;
    border-radius: 0;
    width: 350px;
    height: 80px;
    margin-bottom: 70px;
}
.dropdown-toggle:before {
    top: 0;
    border-top-color: #fff;
}
.dropdown-toggle:before, .dropdown-toggle:after {
    position: absolute;
    display: block;
    content: "";
    border: 25px solid transparent;
    border-top-color: transparent;
}
.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
	color: #ffffff;
	background-color: #0078D7;
	border: none !important;
}
.dropdown-item {
	font-family: Tahoma, sans-serif !important;
	font-weight: 400;
	color: #fff;
	font-size: 1.5rem;
	width: 100% !important;
	display: block;
	position: relative;
	line-height: 1.1 !important;
	padding: 7px 10px !important;
	height: auto;
}
.dropdown-item:hover {
	background-color: #0078D7;
	display: block;
	color: #fff;
	height: auto;
}
.dropdown-item a {
    background-color: #0078D7;
    width: 100% !important;
    color: #fff;
    position: relative;
    display: block;
	height: auto;
}
.dropdown-item a:hover {
    background-color: #0078D7;
    width: 100% !important;
    color: #fff;
    position: relative;
    display: block;
}
.dropdown-menu, .form-group {
	position: relative !important;
	transform: unset !important;
	top: 0 !important;
	left: 0 !important;
	will-change: unset !important;
	line-height: .9 !important;
}
.open > .dropdown-menu {
	display: block !important;
	top: -1px !important;
	left: 0 !important;
	width: 353px !important;
	background-color: #689BC5 !important;
	height: auto;
	border-radius: 0;
}
.down:before {
  border-left: 20px solid transparent;
  border-right: 20px solid transparent;
  border-top: 20px solid #fff;
}
.btn {
	display: inline-block;
	margin-bottom: 0;
	font-weight: normal;
	text-align: center;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	background-image: none;
	border: 1px solid transparent;
	white-space: nowrap;
	padding: 20px 0 12px 12px;
	font-size: 1.5rem !important;
	line-height: 1.42857143;
	border-radius: 4px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	font-family: Raleway, sans-serif !important;
	font-weight: 700;
}
.btn-primary {
    color: #ffffff;
    background-color: #689BC5 !important;
	border: none !important;
}
#wpv-view-layout-14221 {margin-top: 70px;}
.leader-container {width: 320px; height: 450px; float: left;text-align: center;margin: 0 70px 40px 0;}
.leader-photo {width: 320px !important; height: 320px !important; border-radius: 50%;margin-bottom: 15px;}
div.leader-name a {font-size: 18px; color: #000 !important; font-weight: 700 !important;}
.leader-position {font-size: 18px; color: #000 !important; font-weight: 400 !important;
}
a.wpv-filter-pagination-link.js-wpv-pagination-link.page-link, a.wpv-filter-last-link.js-wpv-pagination-last-link, a.wpv-filter-pagination-link.js-wpv-pagination-link.page-link, a.wpv-filter-first-link.js-wpv-pagination-first-link {
		color: #fff !important;
		font-weight: 700;
		z-index: 100;
		background-color: transparent;
		border: none;
}
ul.pagination {
		list-style: none;
}
ul.pagination li  {
		display: inline-block;
		padding: 5px 15px;
		border: 2px white solid;
		background-color: #a0cf67 !important;
		color: #fff !important;
		font-weight: 700;
}
a.wpv-filter-pagination-link.js-wpv-pagination-link.page-link, a.wpv-filter-last-link.js-wpv-pagination-last-link,
a.wpv-filter-pagination-link.js-wpv-pagination-link.page-link, a.wpv-filter-first-link.js-wpv-pagination-first-link {
	color: #fff !important;
	font-weight: 700;
	z-index: 100;
}
.pagination > .active > span {
	background-color: transparent !important;
	border: none !important;
}
.project-category-more-news-title {
	font-family: Raleway, sans-serif;
	font-weight: 700;
	font-size: 48px;
	line-height: 1;
	text-align: left;
	top: 0;	
	display: block;
	position: relative;
}	
.header-main-layout-1 .main-navigation {
    padding: 0 !important;
    vertical-align: top;
}
.wpt-repetitive.wpt-image .wpt-file-preview {
	width: 100%;
	max-width: unset !important;
	background: #fafafa;
	height: auto;
}
#wpforms-13695-field_5-container {
    display: none !important;
    visibility: hidden !important;
}
/* CUSTOM BLUE SELECT */
/*the container must be positioned relative:*/
select {
	min-height: 30px;
	color: white;
	background-color: #689BC5;
	font-family: Raleway, sans-serif;
	font-size: 24px;
	font-weight: 700;
}
.selectop {
  position: relative;
  font-family: Arial;
}
.selectop  select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
/*  background-color: .cvp-live-filter select { */
	min-height: 35px;
	color: #fff;
	background-color: #689BC5;
	font-family: Raleway, sans-serif;
	font-size: 26px;
	font-weight: 700;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  cursor: pointer;
  user-select: none;
  font-weight: 700;
}
/*style items (options):*/
.select-items, option {
  font-size: 24px;
  font-family: Raleway, sans-serif;
	font-weight: 700;
  position: absolute;
  background-color: #689BC5;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: #71CAD0;
  font-size: 26px;
}	
/*********************/
.featured-news-container {
	width: 100%;
	max-width: 565px;
	float: left;
	padding: 0 50px 0 0;
}
.featured-news-image {
	width:100%;
	height:auto;
	max-width:565px;
	padding-top: 35px;
    display: block !important;
    text-align: left !important;
    clear: both !important;
    color: transparent;
}
.featured-news-title {
    font-family: Raleway, sans-serif;
    font-size: 36px !important;
    color: #000000 !important;
    background-color: transparent !important;
    font-weight: 700 !important;
    text-transform: none !important;
    display: block !important;
    text-align: left !important;
    clear: both !important;
    line-height: 1 !important;
	padding: 35px 0 !important;
	max-width: 565px !important;
}
.featured-news-title a {
    padding: 0 !important;
    height: 35px;
	color: #000;
	max-width: 565px !important;
}
.featured-news-excerpt {
	font-size: 18px !important;
	padding: 0 0 20px 0;
	max-width: 565px !important;
}
.news-date {font-size: 13px; display: block;} 
.author {
	font-size: 13px;
	margin: 0;
	color: #7D7D93;
	display: block;
	padding: 0;
}
.author:before {
	content: "Author: "
}
.news-featured-image {
	width: 100% !important;
	height: auto;
	margin-bottom: 70px;
	max-width: 770px;
	max-height: 770px;
	position: relative;
}
.description-checkbox:before {
	content: "<br>"
}
.wpt-form-checkbox-label {
    margin-bottom: 0;
    font-weight: 700 !important;
	display: block;
}	
img {width: 100%;}
#wpcf-group-services-page img {
	max-width: 600px !important;
}
.more-news-title {
	font-family: Raleway, sans-serif;
	font-weight: 700;
	font-size: 48px;
	line-height: 1;
	padding: 0 80px 35px 0;
	text-align: left;
	margin-top: 30px; 
	top: 0;
}
.more-employee-owners {
	font-family: Raleway, sans-serif;
	font-weight: 700;
	font-size: 48px;
	margin: 0 0 90px 0;
	text-align: left;
}
wpforms-13695-field_5-container {
	visibility: hidden;
}
#wpforms-13695 .wpforms-field-container {
	width: 100% !important;
	display: block !important;
}
#wpforms-submit-13695 {
	background-color: #a0cf67 !important;
	color: #fff !important;
	width: 132px;
	font-family: Raleway, sans-serif;
}
#feedback-button {
	color: #fff;;
	background-color: transparent;
	display: inline-block; 
}
.popmake-close {
	background-color: #a0cf67 !important;
}
.pum-theme-11699 .pum-title, .pum-theme-enterprise-blue .pum-title {
	color: #000;
	text-align: left;
	font-size: 38px;
	line-height: 1;
	font-family: Raleway, sans-serif;
	font-weight: 700;
}
.pum-theme-13697 .pum-content + .pum-close, .pum-theme-white-style .pum-content + .pum-close {
	position: absolute;
	height: 35px;
	width: 35px;
	left: auto;
	right: 5px;
	bottom: auto;
	top: 5px;
	padding: 10px;
	color: #fff;
	font-family: Roboto;
	font-weight: 400;
	font-size: 22px;
	line-height: 13px;
	border: 1px none #ffffff;
	border-radius: 25px;
	box-shadow: 2px 2px 3px 0px rgba( 2, 2, 2, 0.00 );
	text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 );
	background-color: rgba( 160, 207, 103, 1.00 );
}
.pum-theme-13697 .pum-container, .pum-theme-white-style .pum-container {
	padding: 20px;
	border-radius: 7px;
	border: 1px none #000000;
	box-shadow: 1px 1px 3px 0 rgba( 2, 2, 2, 0.23 );
	background-color: rgba( 255, 255, 255, 1.00 );
	margin: 15px 0 0 0;
	display: none;
	height: 605px;
}
a.ss-button-facebook {
    background-position: -90px -46px !important;
}
a.ss-button-twitter {
    background-position: 0 -46px !important;
}
a.ss-button-linkedin {
	background-position: -135px -46px !important;
}
a.ss-button-facebook, a.ss-button-twitter, a.ss-button-linkedin {
	background-image: url(https://staging.esassoc.com/wp-content/uploads/2019/04/socialshare.png) !important;
	width: 45px;
	height: 45px;
	background-repeat: no-repeat;
	padding: 0px;
	border: 0px;
	margin: 0 25px 0 0 !important;
	display: inline-block;
}
a.ss-button-facebook:hover {
    background-position: -90px 0 !important;
    transition-delay: 0s;
    transition-duration: 250ms;
    transition-property: all;
    transition-timing-function: ease-in-out;
}
a.ss-button-twitter:hover {
	background-position: 0 0 !important;
	transition-delay: 0s;
	transition-duration: 250ms;
	transition-property: all;
	transition-timing-function: ease-in-out;
}
a.ss-button-linkedin:hover {
	background-position: -135px 0 !important;
	transition-delay: 0s;
	transition-duration: 250ms;
	transition-property: all;
	transition-timing-function: ease-in-out;
}
.ast-container {
	margin-left: auto;
	margin-right: auto;
	padding-left: 20px !important;
	padding-right: 20px !important;
}
/***********************************/
.featured-project-title {
	font-size: calc((100vw / 1180) * 72);
    font-family: Raleway;
    text-align: left;  
	line-height: 1;
	color: #fff !important;
	font-weight: 800;
	background-color: transparent !important;
	z-index: 10000;
	padding: 6vw 0 0 0;
	text-shadow: 2px 2px 5px black;
	max-width: 70vw;
}
.featured-project-caption {
	font-family: Raleway, sans-serif;
	font-size: 18px !important;
	line-height: 1.3 !important;
	color: #ffffff !important;
	font-weight: 600 !important;
	max-width: 35%;
	text-shadow: 2px 2px 5px black;
}
.featured-project-caption::before {
	content: "";
	width: 45px;
	height: 9px;
	background-color: white;
	margin: 25px 0 4vw 0;
	display: block;
	box-shadow: 2px 2px 5px black;
}
.big-green-project-button {
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67;
	color: #fff;
	text-align: center;
	border: 2px solid #fff;
	margin: 35px 0 0 0;
	padding: 13px 25px;
	display: inline-block;
	line-height: 2;
	border: 2px solid #fff;
	letter-spacing: .1em;
	height: 72px;
}
.big-green-project-category-button {
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67;
	color: #fff;
	text-align: center;
	border: 2px solid #fff;
	margin: 10% 15%;
	padding: 13px 25px;
	display: inline-block;
	line-height: 1.1;
	border: 2px solid #fff;
	letter-spacing: .1em;
	height: 84px;
	width: 220px;
}
.recent-projects-grid-wrapper {
	display: grid;
	grid-template-columns: 1fr 1fr;
	grid-template-rows: 1fr;
	column-gap: 50px;
	row-gap: 40px;
}
.recent-projects-box {
	width: 99%;
	border-bottom: 1px solid #95989A !important;
	padding: 3px 0;
	color: #000;
	height: auto;
}
a.recent-projects-box {
	padding: 5px 0;
	color: #000 !important;
	font-weight: 400;
}
a.recent-projects-link {
	color: #000 !important;
	text-decoration: none !important;
	font-weight: 400 !important;
	height: auto;
	width: 97% !important;
	position: relative;
	display: inline-block;
}
a.recent-projects-link::after {
	content: "→";
	color: #000 !important;
	right: 0;
	float: right;
	padding: 0 0 0 10px;
}
.primary-office-photo {
	margin: 0 0 25px 0;
	max-width: 770px;
	width: 100%;
}
.office-box {
	width: 25%;
	height: 300px;
	min-height: 300px;
	float: left;
}
.office-box-title {
	width: 100%;
	height: auto;
	font-weight: 700;
	max-width: 180px;
}
.office-title {
	width: 100%;
	height: auto;
	font-weight: 700;
}
.office-address {
	text-align: center;
	margin: 0 auto;
}
.office-sidebar-title {
	font-weight: 700;
	text-align: center;
	margin: 0 auto;
	display: block;
	position: relative;
	padding-bottom: 20px;
	font-size: 28px;
}
img.office-lead-photo {
	border-radius: 50%;
	height: auto;
	width: 268px;
	padding-bottom: 15px;
}
.traditions-title {
	display: block !important;
	text-align: center !important;
	font-weight: 700;
	font-size: 48px;
	line-height: .9;
	margin: 110px 0 55px 0;
}
.office-photo-container  {
	float: left;
	width: auto;
	display: inline-block;
	margin: 15px;
}
.office-photo {
	width: 100%;
	height: auto;
}
.office-photo-title {
	font-weight: 700;
	padding-top: 5px;
}
.office-photo-caption {
	max-width: 500px;
}
.office-bottom-section {
	display: block;
	height: auto;
	z-index: 200;
	position: relative;
}
.local-links-container {
	height: 260px;
	width: 350px;
	position: relative;
	margin: 0 80px 90px 15px;
	float: left;
}
.local-link-title {
	background-color: #689BC5;
	color: #fff;
	font-weight: 700;
	padding: 14px 0 0 10px;
	height: 54px;
	width: 350px;
	font-size: 21px;
	vertical-align: middle;
	text-transform: uppercase;
	display: inline-block;
	position: relative;
}
.local-link {
	background-color: #71CAD0;
	height: 54px;
	width: 350px;
}
.local-link:hover {
	background-color: #689BC5;
	color: #fff;
}
.local-link a {
	padding: 14px 0 0 10px !important;
	color: #fff;
	font-weight: 500;
	font-size: 20px;
	line-height: 2.5;
}
.local-link a:hover {
	color: #fff;
	background-color: #689BC5;
}
.hentry {
	margin: 0 !important;
}
h1.office-title  {
	line-height: 1.5;
	margin: 50px 0 90px 0!important;
	font-size: calc((100vw / 1180) * 72);
	font-weight: 800;
}
.ast-row {
	margin: 0 auto;
}
/*  box-sizing: border-box */
#i2, #i3, #G2, #G3 {
	display: none;
}
#O2, #O3 {
	background-color: transparent;
}	
.featured-project-wrapper {
	width: 100vw;
	height: calc(100vw * .49); /* the aspect ratio of image */
}
a {
	font-weight: 700;
}
.map-container {
	width: 100vw;
	padding: 80px 0;
	margin: 0 auto;
}
.map-title {
	font-size: calc((100vw / 1180) * 72);
	line-height: 1.1 !important;
	margin: 0;
	font-weight: 800 !important;
	font-family: Raleway, sans-serif;
	text-align: center;
}
.thin-offices {
	font-weight: 300;
	text-align: center;
	display: inline-block;
	font-family: Raleway;
	font-size: 4.1vmin !important;
	color: #6D6371;
	position: relative;
	margin: 28% 0 0 31%;
	z-index: 100;
}
.thin-green-offices {
	font-weight: 300;
	text-align: center;
	display: inline-block;
	font-family: Raleway;
	font-size: 3.4em !important;
	color: #7FBA00;
	position: relative;
	margin: 0 0 0 46%;
}
.round-green-button {
	font-size: 1.9em;
	text-align: center;
	background-color: #a0cf67 !important;
	color: #fff !important;
	border-radius: 100%;
	height: 47px;
	width: 47px;
	position: absolute;
	vertical-align: middle;
	line-height: 1.2;
	bottom: 25%;
	right: 4%;
}
.round-green-button a {
	color: #fff !important;
	background-color: #a0cf67;
	border: 3px solid #fff;
}
.round-green-button a:hover {
	color: #a0cf67 !important;
	background-color: #fff;
	border: 3px solid #a0cf67;
}
.tiny-bio {
	text-align: center;
	margin: 0 auto;
	max-width: 268px;
	width: 100%;
	padding: 0;
	float: none;
	padding: 30px 0;
}
.transparent-button {
	background-color: transparent;
	color: #000 !important;
	border: none !important;
	padding: 0 !important;
	text-align: left;
	line-height: 1.1;
}
.transparent-button:hover, .transparent-button:focus {
	background-color: transparent !important;
}
#wpv-view-layout-9918-CATTR5067c384575b6a83d5861258b0c499f1 {
		height: 400px;
}
.similar-green-button {
	font-size: 35px;
	text-align: center;
	background-color: #a0cf67 !important;
	color: #fff !important;
	height: 40px;
	width: 40px;
	position: absolute;
	vertical-align: middle;
	line-height: 1;
	bottom: 0;
	right: 0;
	margin: 0;
}
.similar-green-button a {
	color: #fff !important;
	background-color: #a0cf67;
	border: 3px solid #fff;
} 
.similar-projects {
	max-height: 380px;
	max-width: 770px;
	width: 100%;
	height: 380px;
	margin-bottom: 40px;
	display: block;
	position: relative;
	float: right;
	display: block;
	position: relative;
}
.similar-project-card {
	background-color: #fff;
	max-height: 120px;
	width: 330px;
	max-width: 330px;
	padding: .5vw;
	position: relative;
	border: 1px solid #ddd;
	min-height: 110px;
	z-index: 100;
	display: block;
	float: right;
	color: #000;
	right: 0;
	top: 8vw;
}
.similar-project-card-title {
	color: #000 !important;
	font-weight: 700;
	display: block;
	line-height: 1.2;
	margin-bottom: 10px;
}
a.similar-project-card-title {
	color: #000 !important;
}
.card-container {
	margin: 0 0 20px 0;
	position: relative;
	height: 125px;
	min-height: 125px;
	min-width: 370px;
	width: 397px;
	right: 0;
	float: right;
	z-index: 500;
}
.project-card {
	height: 125px;
	min-height: 125px;
	min-width: 370px;
	width: 370px;
	background-color: #fff;
	padding: 13px 42px 13px 13px;
	border: 1px solid #DCDDDE;
}
.project-card-orange-border {
	background-color: #F9A134;
	height: 125px;
	max-height: 125px;
	width: 17px;
	position: relative;
	margin: 0 10px 0 0;
	border: none;
	min-height: 125px;
	z-index: 100;
	display: inline-block;
	float: left;
}
.project-title {
	color: #000 !important;
	font-weight: 700;
	text-align: left;
}
.project-card a {
	color: #000;
}
.project-header {
	margin-bottom: 2vw;
}
.project-info {
	margin: -7vw 0 0 0;
}
i.fa.fa-No.Icon {
	display: none;
	visibility: hidden;
}
.tab-title {
	font-weight: 700;
	padding-bottom: 20px;
}
.r-tabs .r-tabs-panel {
	padding: 50px 0 0 0;
}
.tabs-flat .tabs-content {
	background: none repeat scroll 0 0 rgb(256, 256, 256);
	overflow: hidden;
	text-align: left;
}
.tabs-flat ul.tabs-nav li.tabs-nav-items {
	background: #fff;
	padding: 10px 0 !important;
	border-left: none !important;
	margin: 0 40px 0 0 !important;
}	
#responsiveTabs-16937 ul.tabs-nav li.tabs-nav-items, #responsiveTabs-16937 .r-tabs-accordion-title {
    background: #ffffff;
    padding: 0;
    margin: 0 40px 0 0;
}
.single .post-navigation {
    display: none;
    visibility: hidden;
}
.TCP-container {
	padding-bottom: 60px;
	display: block;
	width: 420px;
	max-width: 420px;
	position: absolute;
	right: 0;
	top: 15vw;
}
.TCP-container .grid-wrapper {
    grid-column-gap: 50px !important;
}
img {
    height: auto;
    border: 0;
}
.social-icons img {
	height: 22px;
	width: auto;
	border: 0;
}
.news-image img {
	width: 324;
	height: 324;
	margin-right: 50px !important;
	min-width: 150;
	min-height: 225;
}
.news-author-name {
	font-size: calc((100vw / 1180) * 72);
	line-height: 1.4 !important;
	margin: 0;
	padding: 4.5vw 0 50px 0 !important;
	font-weight: 800 !important;
}
a.news-title, a.news-item-title {
	font-family: Raleway, sans-serif;
	font-size: 36px;
	color: #000;
	background-color: transparent;
	font-weight: 700;
	line-height: 1;
	margin: 0 0 35px 0;
	display: block;
}
.news-item-hero-image {
	width: 100%;
	height: auto;
	z-index: 1;
}
.img-none {height: auto}
.news-item-detail-white-sidebar {
	margin: 0;
	display: inline-block;
	padding: 0;
	background-color: #fff;
}
.news-item-white-title-four-lines {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: calc((100vw / 1180) * -405);
	font-weight: 800;
	background-color: transparent !important;
	line-height: 1.1;
	text-shadow: 2px 2px 5px black;
	width: 950px;
	max-width: 75vw;	
}
.news-item-white-title-three-lines {
    font-size: calc((100vw / 1180) * 72);
    color: #fff !important;
    margin-top: calc((100vw / 1180) * -300);
    font-weight: 800;
    background-color: transparent !important;
    line-height: 1.1;
    text-shadow: 2px 2px 5px black;
	width: 950px;
	max-width: 75vw;
}
.news-item-white-title-two-lines {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: calc((100vw / 1180) * -200);
	font-weight: 800;
	background-color: transparent !important;
	line-height: 1.1;
	text-shadow: 2px 2px 5px black;
	width: 950px;
	max-width: 75vw;
}
.news-item-white-title {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: calc((100vw / 1180) * -150);
	font-weight: 800;
	background-color: transparent !important;
	line-height: 1.2;
	text-shadow: 2px 2px 5px black;
	width: 950px;
	max-width: 75vw;	
}
.news-item-detail-white-details {
	background-color: #fff;
	width: 100%;
	display: inline-block;
	float: left;
	margin: 50px 0;
	max-width: 770px;
	padding: 25px;
	background-color: #fff;
}
.news-item-black-title {
	font-size: calc((100vw / 1180) * 72);
	color: #000 !important;
	margin: 6vw 0 70px 0 !important;
	font-weight: 800;
	background-color: transparent !important;
	max-width: 950px;
	line-height: 1;
}
.news-item-excerpt {
	font-size: 18px;
	line-height: 1.5;
	max-width: 770px;
	display: block;
	background-color: #fff !important;
	padding: 25px;
}
.news-item-details {
	background-color: #fff;
	width: 100%;
	display: inline-block;
	float: left;
	margin: 0 0 90px 0;
	max-width: 770px;
}
.news-item-sidebar {
	margin: 0;
	display: inline-block;
	position: relative;
	float: left;
	margin: 140px 0 0 0;
	background-color: #fff;
}
.news-item-detail-white-sidebar {
	margin: 160px 0 0 -40px;
	display: inline-block;
	padding: 30px 0 0 40px;
	background-color: #fff;
}
.news-item-read-more {
	font-family: Raleway, sans-serif;
	font-size: 24px !important;
	color: #ffffff !important;
	background-color: #a0cf67 !important;
	font-weight: 600 !important;
	text-transform: uppercase !important;
	line-height: 2 !important;
	text-align: center;
	border: 2px solid #fff;
	margin: 20px 0;
	padding: 10px 28px;
	display: block;
	height: 72px;
	width: 310px;
} 
.news-read-more {
	font-family: Raleway, sans-serif;
	font-size: 24px !important;
	color: #ffffff !important;
	background-color: #a0cf67 !important;
	font-weight: 600 !important;
	text-transform: uppercase !important;
	line-height: 2 !important;
	text-align: center;
	border: 2px solid #fff;
	margin: 35px 0;
	padding: 10px 28px;
	display: block;
	height: 72px;
	width: 240px;
}
.news-item-details-read-more {
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67 !important;
	color: #fff;
	text-align: center;
	border: 2px solid #fff !important;
	margin: 35px 0;
	padding: 13px 25px;
	display: inline-block;
	line-height: 2;
	border: 2px solid #fff;
	letter-spacing: .1em;
	height: 72px;
	width: 310px;		
}
.news-item-details-sidebar {
	margin: 20px 0 0 50px;
}
.carrot-top {
    border-top: 4px solid #F9A134 !important;
    padding-top: 18px;
}
.main-header-menu .current-menu-item > a, .main-header-menu .current-menu-ancestor > a, .main-header-menu .current_page_item > a {
	padding-top: 5px !important;
}
a:focus, a:hover {
    color: #000000;
}
.small-header-menu {
	overflow: auto;
	white-space: nowrap;
	font-size: 14px;
	text-align: right;
	line-height: 3;
	overflow-x: auto;
	overflow-y: hidden;
	-webkit-overflow-scrolling: touch;
}
.small-header-menu a {
	color: #6D6E71;
	display: inline-block;
	text-align: right;
	padding: 15px 37.5px 0 0;
	font-weight: 700;
}
.small-footer-menu {
	overflow: auto;
	white-space: nowrap;
	font-size: 14px;
	text-align: right;
	line-height: 3;
	overflow-x: auto;
	overflow-y: hidden;
	-webkit-overflow-scrolling: touch;
}
div.small-footer-menu a {
	color: #6D6E71;
	display: inline-block;
	text-align: center;
	padding: 15px 40.5px 0 0;
	border-top: 1px solid #ADAFB2;
	font-weight: 700;
}
.astra-advanced-hook-10669, .astra-advanced-hook-10670, .astra-advanced-hook-10880 {
    max-width: 1180px;
    display: block;
    text-align: right;
    position: relative;
    margin: 0 auto;
	text-transform: uppercase;
	padding: 0 18px 0 0 !important;
}
.astra-advanced-hook-10732, .astra-advanced-hook-10758, .astra-advanced-hook-10881  {
	max-width: 1180px;
	display: block;
	text-align: right;
	position: relative;
	margin: 0 auto;
	text-transform: uppercase;
} 
.astra-advanced-hook-6793  {
    margin: 0 auto !important;
}
.grid-employee-owner {
	display: grid;
	grid-template-columns: auto min-content;
	grid-template-rows: auto auto;
	grid-column-gap: 50px;
	grid-template-areas: "eo-left-two-thirds eo-right-third";
    /* grid-template-columns: 2fr 33%; */
    grid-auto-flow: dense;
}
.eo-left-two-thirds, .eo-right-third {
	padding: 0;
	margin: 0 auto;
	width: 100%;
}
.employee-name {
		padding-bottom: 70px;
}
.employee-owner-title {
	margin-bottom: 90px;
}
.employee-owner {
	margin: 0 auto;
	text-align: center;
	float: left;
	display: inline-block;
	width: 50%;
	min-width: 250px;
}
.employee-owner-photo {
	border-radius: 100%;
	margin: 0 auto;
	width: 100%;
	max-width: 324px;
}
.employee-owner-hero-photo {
	margin: 0 auto;
	width: 100%;
	max-width: 770px;
	padding: 0 0 40px 0;
}
.employee-owner-name {
	font-weight: 700;
	text-transform: uppercase;
	margin: 15px 0 -20px 0;
}
.employee-owner-position {
    margin: 20px 0 15px 0 !important;
}
.employee-owner-fun-facts, .employee-owner-fun-facts p {
	margin: 0 auto;
	text-align: center;
	width: 80%;
	padding: 0;
	max-width: 325px;
}
.employee-owner-read-more {
	font-family: Raleway, sans-serif !important;
	font-size: 24px !important;
	color: #a0cf67 !important;
	background-color: #ffffff !important;
	font-weight: 600 !important;
	text-transform: uppercase !important;
	text-align: center !important;
	line-height: 2 !important;
	letter-spacing: .1em !important;
}
.employee-owner-spotlight {
	margin: 0 50px 0 0;
	text-align: center;
	float: left;
	display: inline-block;
	width: 28%;
	min-width: 300px;
	min-height: 700px;
}
.employee-owner-spotlight-name {
	font-weight: 700;
	text-transform: none;
	margin: 0 0 35px 0 !important;
	text-align: center;
	padding: 40px 0 0 0;
	font-size: 48px;
	font-family: Raleway, sans-serif;
	line-height: 1.1;
}
.employee-owner-spotlight-photo {
	border-radius: 100%;
	margin: 0 auto;
	width: 324px;
	text-align: center;
}
.employee-owner-spotlight-excerpt {
	text-align: left;
	padding-top: 150px;
}
.employee-owner-spotlight-read-more {
	font-family: Raleway, sans-serif !important;
	font-size: 24px !important;
	color: #a0cf67 !important;
	background-color: #ffffff !important;
	font-weight: 700 !important;
	text-transform: none !important;
	line-height: 2 !important;
	margin: 25px 0 0 0;
	display: block;
}
.more-eo-spotlights {
	font-size: 48px;
	margin: 110px 0 90px 0;
	font-weight: 800;
	line-height: 1.1;
}
.owner-header {
	font-size: 60px; /* calc((100vw / 1180) * 60); */
	font-family: Raleway;
	font-weight: 800;
	line-height: 1;
	text-align: center;
	vertical-align: middle;
	padding: 50px 0;
}
.join-us-section {
	margin-top: 100px;
}
.section-spacer {
	padding: 140px 0 60px 0;
	display: block !important;
	position: relative;
	height: auto;
}
.small-section-spacer {
	padding-top: 80px;
}
.small-section-spacer-top-bottom {
	padding: 80px 0;
}
.special-spacer {
	padding-top: 100px;
	display: block;
	margin-top: 400px;
}
.section-spacer-100-0 {
	padding: 100px 0;
}
.section-spacer-60-0 {
	padding: 60px 0;
}
.top-section-spacer {
	padding: 140px 0 30px 0;
}
.section-spacer-top-100 {
	padding-top: 100px;
}
.section-spacer-0-0-100-0 {
	padding: 0 0 100px 0;
}
.projects-spacer, .services-spacer {
	padding: 40px 0 100px 0;
}
.leadership-details {
	margin-top: -5vw !important;
	padding: 20px !important;
	background-color: #fff !important;
	width: 60vw;
	display: inline-block;
	min-height: 700px;
	max-width: 770px;
	float: left;
	z-index: 300;
}
.leadership-large-image {
	z-index: 1;
}
.leadership-sidebar {
	padding: 0;
	display: inline-block !important;
	float: left !important;
	margin: 3vw 0 0 ;
	position: relative;
}
.leadership-image {
	width: 100%;
	height: auto;
	margin-top: 70px;
}
.white-leadership-name {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: -12vw !important;
	font-weight: 800;
	background-color: transparent !important;
	text-shadow: 2px 2px 5px black;
	display: block;
	z-index: 300;
}
.employee-owner-details  {
    padding: 0 !important;
    background-color: #fff !important;
    width: 60vw;
    display: inline-block;
	min-height: 700px;
	max-width: 800px;
	float: left;
}
.giant-details {
	margin-top: -1.5vw;
	padding: 0;
	background-color: #fff;
	width: 60%;
	display: inline-block;
	max-width: 900px;
}
.project-details-info {
	margin-top: 80px;
}
.project-details-titles {
	font-family: Raleway;
	font-size: 36px;
	font-weight: 700;
	margin: 0 0 40px 0;
}
.project-category-details {
	margin: 3vw 0 130px 0; 
	padding: 25px !important;
	background-color: #fff;
	width: 64%;
	display: inline-block;
	max-width: 900px;
	height: auto;
	min-height: 1500px;
}
.project-category-sidebar {
	text-align: center;
	/* padding: 0 20px 0 ; */
}
.project-details-details {
	background-color: #fff;
	padding: 25px;
	margin: 35px 0 54px 0;
	min-height: 950px;
}
/* Should be margin top of 8vw */
.project-details-sidebar {
	margin: 12vw 0 100px 0;
	padding: 0;
}
.project-details {
	margin: 6vw 0 130px 0; 
	padding: 25px !important;
	background-color: #fff;
	width: 64%;
	display: inline-block;
	max-width: 900px;
	height: auto;
}
.project-details-image-1 {
		position: absolute;
		left: 0;
		width: 100%;
		height: auto;
		max-width: 1120px;
}
@media screen and (max-width: 1880px) {	
	.project-details-image-1 {
		left: 0;
		width: 100%;
		height: auto;
		max-width: 770px;
	}
}
@media screen and (max-width: 1250px) {	
	.project-details-image-1 {
		position: relative; 
		left: 0;
		width: 100%;
		height: auto;
		max-width: 770px;
	}
	.project-details-details {
		min-height: 800px;
	}
	.news-item-excerpt {
		padding: 25px 0 !important;
	}
}
.project-quote-photo {
	max-width: 268px;
	display: block;
	margin: 0 auto;
	padding-top: 25px;
}
.project-matters-quote {
	border-left: 4px double #699cc6;
	padding: 0 0 0 4vw;
	max-width: 650px;
	float: left;
	height: 325px;
	width: 50vw;
}
.matters-quote {
	width: 100%;
	padding-top: 25px;
}
.title-of-quoted {
	margin-bottom: 60px;
}
.blue-quotes {
	font-size: 180px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	color: #699cc6;
	padding-left: 25px;
}
.project-sidebar {
	text-align: left;
	padding: 0 0 0 40px;
	width: 32%;
	display: inline-block !important;
	float: none !important;
	margin: 14vw 0 0 0;
	position: relative;
	vertical-align: top;
	line-height: 1.5;
}
.hero-project-image {
	width: 100%;
	height: auto;
}
.white-project-name-four-lines {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: calc((100vw / 1180) * -405); 
	font-weight: 800;
	max-width: 950px;
	background-color: transparent !important;
	line-height: 1.1;
	text-shadow: 2px 2px 5px black;
}
.white-project-name-three-lines {
    font-size: calc((100vw / 1180) * 72);
    color: #fff !important;
    margin-top: calc((100vw / 1180) * -255);
    font-weight: 800;
    max-width: 950px;
    background-color: transparent !important;
    line-height: 1.1;
    text-shadow: 2px 2px 5px black;
}
.white-project-name-two-lines {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: calc((100vw / 1180) * -200);
	font-weight: 800;
	max-width: 950px;
	background-color: transparent !important;
	line-height: 1.1;
	text-shadow: 2px 2px 5px black;
}
.white-project-name {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	margin-top: calc((100vw / 1180) * -150);
	font-weight: 800;
	max-width: 950px;
	background-color: transparent !important;
	line-height: 1.2;
	text-shadow: 2px 2px 5px black;
}
.project-sidebar {
	text-align: left;
	padding: 0 0 0 40px;
	width: 32%;
	display: inline-block !important;
	float: none !important;
	margin: 14vw 0 0 0;
	position: relative;
	vertical-align: top;
	line-height: 1.5;
}
h2.project-sidebar, h2.project-category-sidebar {
	line-height: 1;
}
.bios-sidebar  {
    padding: 20px 0 0 20px;
    width: 35vw;
    display: inline-block !important;
    float: left !important;
    margin: 180px 0 0 10px;
	position: relative;
	max-width: 325px;
}
.experts-title {
	font-size: 36px;
	font-weight: 700;
	text-align: center;
}
.service-experts {
	float: none;
	display: block;
	text-align: center;
	margin: 0 auto;
}
.service-sidebar {
	padding: 0 !important;
	display: inline-block !important;
	float: left !important;
	margin: 40px 0 70px 0 !important;
	position: relative !important;
	min-height: 500px;
	text-align: center !important;
}
.service-details {
	margin: -65px 0 0 0;
	background-color: #fff !important;
	max-width: 770px;
	display: inline-block;
	min-height: 700px;
	float: left !important;
	position: relative;
	padding: 25px !important;
	width: 59vw;
}
.service-title {
	font-size: 48px;
	font-weight: bold;
	line-height: .9;
	margin: 30px 0 35px 0;
}
.service-featured-project {
	text-align: center;
	margin: 0 auto;
	width: 100%;
}
.service-featured-project-caption {
	margin-top: 30px;
	text-align: center;
	display: block;
}
.child-pages-link {
	display: block;
	line-height: 1.6;
}
.service-featured-project-container {
	height: auto !important;
	width: 100%;
	max-width: 360px;
	position: relative;
	display: inline-block;
}
.service-featured-project-image, .service-featured-project-title {
	display: block;
	height: auto;
	width: 100%;
}
.service-featured-project-button {
		display: block;
		height: auto;
		margin: 0 auto;
}
.service-featured-project-title {
	padding: 10px 0 0 0;
	height: 64px;
	line-height: 1;
}
.giant-image {
	width: 100%;
	height: auto;
}
* {
  box-sizing: border-box !important;
}
.service-icon {
	max-width: 234px;
	width: 234px;
	height:234px;
	float: right;
	display: block;
	padding-top: 20px;
}
#giant-categories, #giant-icon-column {
    margin: 25px 0 45px 0;
    width: 47%;
    float: left;
	display: inline-block;
}
#giant-categories a {
	border-top: 1px solid #ddd;
	width: 320px !important;
	display: block;
}
h1.giant-white-title {
	font-size: calc((100vw / 1180) * 72);
	color: #fff !important;
	font-weight: 800;
	background-color: transparent !important;
	z-index: 10000;
	padding: 6vw 0 0 0;
	margin: 0 auto;
	text-shadow: 2px 2px 5px black;
	line-height: 1 !important;
}
.ast-sticky-active {
	position: fixed !important;
	right: 0;
	left: 0;
	margin: 0 auto;
	width: 100%;
	z-index: 5000;
}
.ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img img {
    max-height: 67px !important;
    width: 180px;
}
.img-circle {
	border-radius: 100%;
	padding-bottom: 15px;
}
.search-custom-menu-item {
	float: right
}
input[type="search"] {
    color: #222 !important;
    padding: 8px 0 2px 10px;
    height: auto;
    border-width: 1px;
    border-style: solid;
    border-color: #888;
    background: #fff;
    background-color: rgb(255, 255, 255);
    box-shadow: none;
    box-sizing: border-box;
    transition: all .2s linear;
}
.ast-search-menu-icon.slide-search .search-form {
	-webkit-backface-visibility: visible;
	backface-visibility: visible;
	visibility: hidden;
	opacity: 0;
	transition: all .2s;
	position: absolute;
	z-index: 1300;
	right: -1em;
	top: 50%;
}
.ast-search-icon .astra-search-icon {
    font-size: 1.5em;
    float: right;
	z-index: 150;
}
li.ast-masthead-custom-menu-items {
    padding: 0 !important;
	margin: 0 auto !important;
}
.tabs-nav {
	margin: 0 auto;
	padding: 0;
}
ul.tabs-nav li.r-tabs-state-active, .r-tabs-state-activee {
    background: #ffffff;
    border-bottom: 4px solid #F9A134 !important;
	border-right: none !important;	
	border-left: none !important;
}  
li.tabs-nav-items .r-tabs-tab .r-tabs-state-active {
	border-bottom: 4px solid #F9A134;	
}
.tabs-content {
    display: block;
    min-height: 400px;
}
.listing-item {margin: 25px 0;}
.excerpt-more {
	display: block;
	font-weight: 700;
}
.attachment-thumbnail {
	height: auto;
	width: 100%;
	margin: 0 auto;
	text-align: center;
}
.ddl-full-width-row > [class*="col-"]:first-child {
    padding-left: unset;
}
.icons-section {
	float: left;
	position: relative;
	display: inline-block;
	max-width: 234px;
	margin: 4vw 30px;
	overflow-wrap: normal;
	min-width: 234px;
}
.icons:hover {
	fill: #421953;
}
.services-title {
	font-weight: 400 !important;
	font-family: Raleway, sans-serif;
	text-align: center !important;
	color: #000 !important;
	margin-top:20px;
	overflow-wrap: normal;
	height: 55px;
}
.project-titles {
	font-size: 36px;
	font-weight: 700;
	padding: 0 0 30px 0;	
	line-height: 1.1;
}
h4, .entry-content h4, .entry-content h4 a {
    font-size: 26px !important;
	font-weight: 400 !important;
}
.thirty-six-bold {
	font-size: 36px;
	font-weight: 700;
	padding: 0 0 30px 0;
}
.thirty-two-bold {
	font-size: 32px;
	font-weight: 700;
	padding: 0 0 30px 0;
}
.single .entry-header {
    margin: 0;
	height: 0;
}
body {font-family: Raleway, sans-serif;}
ol, ul {
  margin: 0 0 1.5em 1em;
  margin-bottom: 1.5em;
}
.ast-small-footer-section {
	padding: 0;
}
.footer-sml-layout-2 .ast-small-footer-section-2 {
    text-align: left;
}
.widget_custom_html, .footer-widget-title {
	color: #fff !important
}
.widget_nav_menu li {
    transition: all .2s linear;
    line-height: 1.7;
}
.footer-adv-overlay {
    padding-top: 50px;
    padding-bottom: 70px;
}
.main-header-bar, .ast-header-break-point .main-header-bar, .main-header-bar-navigation {
    padding: 0;
    height: 130px;
    line-height: 1;
}
.main-header-bar .main-header-bar-navigation {
	text-transform: uppercase;
	border-bottom: 1px solid #ADAFB2;
}
.main-header-menu a {
    text-decoration: none;
    padding: 0;
    display: inline-block;
    transition: all .2s linear;
}
#primary-menu {
	margin-top: 0;
}
.main-navigation ul li a {
    padding: unset;
}
.ast-header-break-point .header-main-layout-1 .site-branding {
    padding-right: 0;
}
header .current_page_item, .current-page-ancestor {
	border-bottom: 5px solid #f9a134 !important;
}
.ast-header-break-point .main-navigation li {
	text-align: left;
	width: 100%;
	margin: 0;
}
.ast-flyout-menu-enable.ast-header-break-point .main-header-bar .main-header-bar-navigation #site-navigation, .ast-flyout-menu-enable.ast-header-break-point .ast-primary-menu-disabled .ast-merge-header-navigation-wrap {
	width: 80%;
	z-index: 5 !important;
}
.custom-logo {
	width: 190px;
	height: 62px;
}

.wpforms-field-label {
    display: inline-block !important;
    font-weight: 700 !important;
    font-size: 16px !important;
    float: left !important;
    line-height: 1.3 !important;
    margin: 12px 4px 0 0 !important;
    padding: 0;
}
.wpforms-form {
	width: 100% !important
}
.wpforms-field-container {
	width: calc(100% - 100px) !important;
	max-width: 580px !important;
	display: inline-block !important;
	margin: 0 auto !important;
	float: left !important;
}
div.wpforms-container-full .wpforms-form .wpforms-required-label {
	display: none;
	visibility: hidden;
}
div.wpforms-container-full .wpforms-form label.wpforms-error {
	display: block;
	color: red;
	font-size: 12px;
	float: none;
	margin: 4px 0 0 0;
}
div.wpforms-container-full .wpforms-form button[type="submit"] {
	background-color: transparent !important;
	color: #fff !important;
	border: 2px solid #fff !important;
	padding: 10px 12px !important;
	text-transform: uppercase;
	float: left !important;
	font-size: 18px !important;
	font-weight: 700;
}
.wpforms-submit-container {
    padding: 0;
    display: inline-block !important;
    float: left !important;
    clear: unset !important;
}
.orange-text {
	color: #f9a134;
	font-weight: 400 !important;
}
.thin-highlight {
	font-family: Raleway;
	font-weight: 300 !important;
	font-size: 4.2vw;
	text-align: center;
	margin: 0 auto;
	padding: 150px 0;
	max-width: 1200px;
	line-height: 5.5vw;
	color: #6D6371;
}
.sub-arrow {visibility: hidden; display: none;}
.ast-single-post .entry-title, .page-title, h1, .entry-content h1 {
	font-size: calc((100vw / 1180) * 72);
	line-height: 1 !important;
	margin: 0;
	padding-top: 5vw !important;
	font-weight: 800 !important;
}
h2, .entry-content h2, .entry-content h2 a {
    font-size: 60px;
	font-weight: 700;
}

.search .entry-title a {
		font-weight: 700;
		font-size: 36px;
		color: #a0cf67;
		line-height: 1;
}
.search .entry-title {
    line-height: 1;
}
.ast-archive-description .ast-archive-title {
	font-family: 'Raleway', sans-serif;
	text-transform: none;
	color: #000;
}
h3, .entry-content h3, .entry-content h3 a {
    font-size: 36px;
	font-weight: 800;
}
.footer-widget-title {
	margin: 25px 0 15px 0 !important;
	font-weight: 700 !important;
	display: inline-block !important;
	color: #fff;
	font-size: 18px;
}
.widget {margin: 0 !important;}
.footer-adv .widget > :not(.widget-title)  {
	font-size: 15px;
}
.footer-adv-widget {padding: 0 !important;
}
.layout-top {
	margin-bottom: 30px;
}
p {
	text-align: left;
	color: inherit;
}
table, td, th {
	border: none;
}
.site-header .ast-site-identity {
    padding: 15px 0;
}
.site-footer a {
    color: #eaeaea;
    font-weight: 400;
}
.ast-desktop .main-header-menu.submenu-with-border .sub-menu, .ast-desktop .main-header-menu.submenu-with-border .children, .ast-desktop .main-header-menu.submenu-with-border .astra-full-megamenu-wrapper {
    border-color: #F9A134;
}
.ast-primary-sticky-header-active .main-header-menu li.current-menu-item > a, .ast-primary-sticky-header-active .main-header-menu li.current-menu-ancestor > a, .ast-primary-sticky-header-active .main-header-menu li.current_page_item > a {
    /* color: #000000; */
    font-weight: 600;
}
#site-navigation {
	height: 130px;
}
#primary {
	margin: 0;
	padding: 0;
}
.center-text {
	text-align: center !important;
	margin: 0 auto;
}
li.ast-masthead-custom-menu-items {
    padding: 10px !important;
}
.ast-flyout-menu-enable.ast-header-break-point .main-header-bar-navigation #site-navigation {
	color: #fff !important;
	background-color: #f9a134;
	opacity: 1;
	z-index: 5;
}
.ast-header-break-point .main-navigation ul li a {
	color: #fff !important;
}
.ast-flyout-menu-enable.ast-header-break-point .main-header-bar-navigation .close::after {
	color: #fff;
	z-index: 5;
}
.container-fluid {
	margin: 0;
	padding: 0;
	height: auto;
	position: relative;
}
.full-width {
  display: block;
  width: 100vw;
  position: relative;
  left: 50%;
  right: 50%;
  margin-left: -50vw;
  margin-right: -50vw;
}
.full-width-teal {
	background-color: #72ccd2;
	min-height: 300px;
	position: relative;
	display: block;
	padding-top: 40px;
}
.full-width-white {
	background-color: #fff;
}
.full-width-grey {
	background-color: #EDEDED;
}
.white-header {
	font-family: Raleway, sans-serif;
	font-weight: 700;
	font-size: 36px;
	color: #fff;
	padding-bottom: 25px;
	margin-top: 40px;
}
.always-looking {
	font-size: 36px;
	font-weight: bold;
	line-height: 1.2;
	padding: 0 0 30px 0;
}
.green-text-button {
    font-family: Raleway, sans-serif;
    font-size: 24px !important;
    color: #a0cf67 !important;
    background-color: #ffffff !important;
    font-weight: 600 !important;
    line-height: 1.2 !important;
}
.blue-divider {
	border-top: 4px double #72CCD2;
	margin: 55px 0 70px 0 !important;
	width: 100% !important;
	display: block;
	max-width: 1180px;
	padding: 0 !important;
}
.blue-divider-190 {
	width: 190px;
	text-align: center;
	border-bottom: 4px double #72CCD2;
	display: block;
	position: relative;
	padding-bottom: 40px;
	margin-top: 30px;
	margin-bottom: 45px;
	margin-left: auto;
	margin-right: auto;
}
.blue-divider-215 {
	max-width: 215px;
	text-align: left;
	margin: 25px 0 !important;
}
.giant-blue-divider {
    border-top: 4px double #72CCD2;
    margin: 50px 0 !important;
    width: 100% !important;
    display: block;
	max-width: 1180px;
}
.thick-white-divider {
	width: 35px; 
	height: 5px; 
	background: #fff; 
}
.ast-separate-container .ast-article-single {
	padding: 0 !important;
}
.ast-header-break-point .ast-mobile-menu-buttons .ast-button-wrap .ast-mobile-menu-buttons-minimal {
	font-size: 3em;
}
.main-header-menu .menu-item {
	text-align: left;
	margin: 0 40px 0 0;
	font-family: Raleway, sans-serif;
}
.main_image {
    position: relative;
    display: inline-block;
    left: 25%;
    max-width: 95%;
}
.overlay_image {
    position: absolute;
    top: 0;
    right: 240px;
    max-width: 30%;
}
.split-wrapper {
	display: grid;
    grid-gap: 10px;
	grid-template-columns: repeat(2, 1fr);
}
.big-green-about-us {
	margin: -40px 0 40px 0;
	right: 30px;
	float: right;
	position: relative;
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67;
	padding: 18px 15px;
	color: #fff;
	text-align: center;
	border: 2px solid #fff !important;
	display: inline-block;
	height: 72px;
	width: 263px;
	letter-spacing: .1em;
}
.split-container {
	max-width: 100%;
    margin-left: auto;
    margin-right: auto;
    padding-left: 20px;
    padding-right: 20px;
}
.green-letters-more {
    font-family: 'Raleway', sans-serif !important;
    font-size: 24px !important;
    color: #a0cf67 !important;
    background-color: #ffffff !important;
    font-weight: 600 !important;
    line-height: 1.2 !important;
	text-align: center;
	margin: 0 auto;
	display: inline-block;
}
.big-green-button, .big-green-button a {
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67 !important;
	color: #fff;
	text-align: center;
	border: 2px solid #fff !important;
	margin: 30px 0;
	padding: 13px 25px;
	display: inline-block;
	line-height: 2;
	border: 2px solid #fff;
	letter-spacing: .1em;
}
.big-green-services-button, .big-green-services-button a {
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67 !important;
	color: #fff;
	text-align: center;
	border: 2px solid #fff !important;
	margin: 20px 0 40px ;
	padding: 13px 25px;
	display: inline-block;
	line-height: 2;
	border: 2px solid #fff;
	letter-spacing: .1em;
	position: relative;
	width: 155px;
	height: auto;
}
.big-green-clients-button {
	font-size: 24px;
	font-family: Raleway, sans-serif;
	font-weight: 700;
	text-transform: uppercase;
	background-color: #A0CF67 !important;
	color: #fff !important;
	text-align: center;
	border: 2px solid #fff !important;
	text-align: center;
	margin: 15% 20%;
	padding: 13px 25px;
	display: block;
	height: 72px;
	width: 250px;
}
.big-green-button:hover, .big-green-about-us:hover  {
	background-color: #64912e !important;
	color: #fff !important;
}
.our-history-grid {
    margin: 0 auto;
    padding: 0 30px;
}
.history-item {
    clear: both;
}
.history-year {
    font-weight: bold;
    font-size: 20px;
    float: left;
    max-width: 20%;
    min-width: 160px;
    display: inline-block;
}
.history-description {
    float: left;
    max-width: 800px;
    padding-bottom: 30px;
}
.sixty-extrabold {
	font-size: 60px;
	font-weight: 800;
	text-align: left;
	display: block;
	line-height: .9;
	padding-bottom: 20px;
}
.social-icons {
    padding: 25px 0 0 40px;
    background-color: transparent;
    border-left: 7px double #707070;
    height: 93px;
    min-width: 320px;
    position: relative;
    display: inline-block;
	margin-top: -16px;
}
.social-icon {
    height: 22px;
    margin: 17px 45px 0 0;
}
input[type="search"] {
	border-radius: none !important;
}

/*******************************/
/***     CSS GRID CODE      ****/
/*******************************/
.grid-wrapper {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto auto;
  grid-gap: 0 50px;
  grid-template-areas: "left-half right-half";
}
.left-half {
  grid-area: left-half;
}
.right-half {
  grid-area: right-half;
}
.grid-wrapper-equal-thirds {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr; 
  grid-template-rows: auto auto auto;
  grid-gap: 0 50px;
  grid-template-areas: "one-third";
}
.one-third {
  padding: .5em;
  margin-bottom: 20px;
}
.grid-wrapper-equal-fourths {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: auto auto auto auto;
  grid-gap: 0 50px;
  grid-template-areas: "one-fourth one-fourth one-fourth one-fourth";
}
.one-fourth {
  padding: .5em;
  margin-bottom: 20px;
}
.grid-wrapper-left-two-thirds {
  display: grid;
  grid-template-columns: 66% 1fr;
  grid-column-gap: 50px;
  grid-template-areas: "left-two-thirds right-one-third";
}
.left-two-thirds, .right-one-third, .left-one-third, .right-two-thirds {
  margin: 0 0 50px 0;
}
.grid-wrapper-right-two-thirds {
	display: grid;
	grid-template-columns: 1fr 2fr;
	grid-template-rows: auto auto;
    grid-column-gap: 50px;
	grid-template-areas: "left-third right-two-thirds";
}
.grid-wrapper-right-two-thirds-similar {
  display: grid;
  grid-template-columns: 1fr 2fr ;
  grid-template-rows: auto auto;
  grid-gap: 0 50px;
  grid-template-areas: "left-third right-two-thirds";
}
.left-third-similar, .right-two-thirds-similar {
  margin-bottom: 20px;
}
.join-us-large-circle {
	width: 65vw;
	height: auto;
	transition: none 0s ease 0s;
	margin: -420px 0 0 -2vw;
	max-width: 690px;
}
.join-us-small-circle {
	max-width: 450px;
	width: 35vw;
	height: auto;
	margin-left: 51%;
}
#slide-1-layer-4 {
	margin-bottom: 50px !important;
}
.more-projects {		
	font-size: 48px !important;
	line-height: 1;		
	font-weight: 800;
	font-family: Raleway, sans-serif;
	/* width: 100%; */
	margin: 0 0 45px 0;
	display: block;
}
.more-projects a {
    font-weight: 500;
    color: #000;
	width: 100% !important;
    border-bottom: 1px solid #ADAFB2;
    margin: 4px 0;
	padding: 7px 0;
    line-height: 1;
	font-size: 18px;
	display: block;
}
.more-projects a:after {
	content: "→";
	float: right;
}
.two-columns, two-columns a {
	line-height: .8 !important;
	padding: 0;
}
	
#top-map-container {
	width: 100%;
	height: 600px;  
}
.project-category-image {
	position: absolute;
	right: 0;
	margin: 50px 0 300px 0;
	min-height: unset;
	width: 100%;
	min-height: 487px;
	padding-bottom: 45px;
	max-width: 770px;
}
.project-category-expert-image, .project-expert-image {
	height: auto;
	max-width: 268px;
	margin: 25px 0;
	text-align: center;
	border-radius: 100%;
}
@media screen and (min-width: 1650px) {
	.project-category-image {
		position: absolute;
		right: 0;
		margin: 50px 0 200px 0;
		min-height: unset;
		width: 100%;
		min-height: 487px;
		padding-bottom: 45px;
	}
}
@media screen and (min-width: 1401px) {
	.grey-float-right {
		float: right;
		right: 0 !important;
		margin: 30px -20vw 0 0;
		width: 70vw;
	}
	.news-item-white-title-four-lines {
		margin-top: -450px;
	}
	.news-item-white-title-three-lines {
		margin-top: -380px;
	}
	.news-item-white-title-two-lines {
		margin-top: -310px;
	}
}
@media screen and (max-width: 1400px) {
	.grey-float-right {
		float: right;
		right: 0 !important;
		margin: 30px -155px 0 0;
		width: 70vw;
	}
}
@media screen and (max-width: 1280px) {
	.grey-float-right {
		float: right;
		right: 0 !important;
		margin: 30px -50px 0 0;
		width: 80vw;
	}
}
@media screen and (min-width: 1241px) {
	.ast-single-post .entry-title, .page-title, h1, .entry-content h1, 
	.featured-project-title, .white-leadership-name, .news-item-white-title, .news-item-white-title-two-lines, .news-item-white-title-three-lines, .news-item-white-title-four-lines, .news-item-black-title, .map-title, .news-author-name {
		font-size: 72px !important;
	}
	.giant-white-title, .white-project-name, .white-project-name-two-lines, .white-project-name-four-lines, .white-project-name-three-lines  {
		font-size: 72px !important;
	}
	.owner-header {
		font-size: 60px;
	}
  	.white-project-name-four-lines {
		margin-top: calc((100vw / 1180) * -305); 	
	}
	.white-project-name-three-lines {
    	margin-top: calc((100vw / 1180) * -255);
	}
	.white-project-name-two-lines {
    	margin-top: calc((100vw / 1180) * -205);
	}
	.white-project-name {
    	margin-top: calc((100vw / 1180) * -150);
	} 
}
@media screen and (max-width: 1240px) {
	.news-item-white-title-three-lines {
		font-size: calc((100vw / 1180) * 72);
		color: #fff !important;
		margin-top: calc((100vw / 1180) * -400);
		font-weight: 800;
		background-color: transparent !important;
		line-height: 1.1;
		text-shadow: 2px 2px 5px black;
		width: 950px;
		max-width: 75vw;
	}
@media screen and (max-width: 1000px) {
	.history-year {
		display: block;
		max-width: unset;
		width: 100%;
	}
	.grey-float-right {
		margin: 50px 0;
		width: 100%;
	}
	.grey-float-content {
		padding: 25px;
		width: 100%;
	}
	.TCP-container {
		padding-bottom: 60px;
		display: block;
		width: 420px;
		max-width: 420px;
		position: relative;
		right: 0;
		top: 5vw;
	}
}

@media (530px <= width <= 1180px ) {
	.join-us-small-circle {
		margin-left: 62% !important;
	}
	.join-us-large-circle {
		margin: -340px 0 0 -10px !important;
	}
}
@media screen and (max-width: 1180px) {
	.project-info {
		margin: 0; 
	}
	.project-category-image {
		position: absolute;
		right: 0;
		margin: 0; 
		min-height: 487px;
		padding-bottom: 45px;
	}
	#giant-categories, #giant-icon-column {
		width: 100%;
    	display: block;
	}
	#giant-icon-column {
		max-width: 324px;
	}
	.featured-news-container {
		padding: 0;
	}
	.white-project-name, .white-project-name-two-lines, .white-project-name-three-lines, .white-project-name-four-lines {
		max-width: 75vw;
	}
	.white-project-name-three-lines {
		max-width: 75vw;
		margin-top: -30vw !important;
	}
	.news-item-detail-white-sidebar {
		margin: 110px 0 0 -40px;
		display: inline-block;
		padding: 30px 0 0 40px;
		background-color: #fff;
	}		
}
@media screen and (max-width: 1050px) {
	.grid-wrapper-right-two-thirds-similar {
			display: block;
	}
	.main-header-menu .menu-item {
		margin: 0 30px 0 0;
	}
}
@media screen and (max-width: 1000px) {
	.office-grid-wrapper {
		display: grid;
		grid-template-columns: repeat(3, 1fr);
		grid-template-rows: repeat(6, 1fr);
		column-gap: 25px;
		grid-row-gap: 40px;
		order: 1;
	}
	.office-box {
		width: 33%;
	}
	.office-photo-container {
		margin: 30px 0;
	}
}
@media (768px <= width <= 1050px) {
	.description {
	    width: calc(100vw - 250px);
	}
}
@media (max-width: 970px) {
/*	.news-item-white-title {
		max-width: 97%;
	} */
	.news-item-white-details {
		background-color: #fff;
		width: 100% !important;
		display: block !important;
		float: none !important;
		min-width: unset !important;
		margin: 0 !important;
		max-width: unset !important;
		padding: 0 !important;
	}
	.news-item-sidebar {
		display: block !important;
		float: none !important;
		padding: 0 !important;
		background-color: #fff !important;
		width: 100% !important;
		margin: 50px 0 50px !important;
	}
} 
@media screen and (max-width: 912px) {
	.blue-quotes {
		padding-left: 15px;
		font-size: 155px
	}
	.project-info {
		margin-top: 40px;
	}
	.white-title-bar {
		width: 100%;
		margin: 0 !important;
		border: 1px solid #aaa;
		height: 42px;
	}
}
@media screen and (max-width: 875px) {
	.news-item-white-title {
		font-size: calc((100vw / 1180) * 72);
		color: #fff !important;
		margin: -24vw 0 0 0 !important;
	}
	#top-map-container {
		height: 75vw;
	}
	.icons-section {
		width: 33%;
	}
	.TCP-container {
		float: none;
		width: 100%;
		position: relative;
		right: 0;
		top: 0;
		text-align: right;
		margin: 0 auto;
		display: block;
		height: 450px;
	}
	.left-third, .middle-third, .right-third, .one-fourth {
		display: block !important;
	    max-width: 100%;
	    width: 100%;
		padding-bottom: 35px;
	}
	.left-two-thirds, .right-one-third, .one-third,  .just-one-half, .left-half, .right-half, .one-fourth {
		width: 100%;
		display: block;
	    margin: 0 0 50px 0 !important;
		border: none;
	}
	.grid-wrapper {
		display: block;
	}
  	.left-half, .right-half {
    	margin: 0px;
  	}
	.grid-wrapper-equal-thirds {
		display: block;
	}
	.left-third, .middle-third {
		margin: 0px;
	}
	.social-icons {
		border: 0;
	}
	.social-icons:before {
		border: none;
	}
	.full-width {
    	padding: 0;
	}
	#primary {
		margin: 0 auto !important;
    	padding: 0 !important;
 	}
	.one-half, just-one-half, .featured-news, .featured-ideas {
		width: 100% !important;
		margin: 0 !important;
		padding: 0;
		display: block !important;
		height: auto !important;
	}
	.service-details {
		width: 100%;
		margin: 0 auto;
		padding: 10px 0 50px 0 !important;
		display: block !important;
	}
	.service-sidebar {
		width: 100%;
		margin: 0 auto;
		padding-bottom: 50px !important;
		text-align: center;
	}
}
@media screen and (max-width: 787px) {
	.project-matters-quote {
		border-left: 4px double #699cc6;
		padding: 0 0 0 3vw;
		max-width: 650px;
		float: left;
		height: 325px;
	}
	.r-tabs .r-tabs-accordion-title {
		display: block;
		height: 44px;
		border-top: 1px solid #cdcdcd;
		padding: 10px 0 !important;
		width: 100%;
		border-bottom: none;
	}
	.big-green-about-us {
		margin: -10px 0 0 0;
		right: 3%;
		text-align: center;
		display: block;
	}
}
@media screen and (max-width: 768px) {
	.ast-header-break-point .main-header-menu li.current-menu-item > a, .ast-header-break-point .main-header-menu li.current-menu-ancestor > a, .ast-header-break-point .main-header-menu li.current_page_item > a {
		background-color: #F9A134;
		padding-top: 0 !important;
	}
	#menu-item-13775 {
		border-bottom: none !important;
		padding-top: 0 !important;
	}
	.main-header-menu .current-menu-item > a, .main-header-menu .current-menu-ancestor > a, .main-header-menu .current_page_item > a {
		padding-top: 0 !important;
	}
}
@media screen and (max-width: 767px) {
	h1.office-title  {
		line-height: 1.5;
		margin: 35px 0 50px !important;
		font-size: 36px !important;
		font-weight: 800;
	}
	.leader-container {
		display: block;
		width: 350px;
		height: 450px;
		float: none;
		text-align: center;
		margin: 0 auto;
	}
	.employee-owner-spotlight {
		margin: 0 auto;
		text-align: center;
		float: none;
		display: block;
		width: 28%;
		min-width: 300px;
		min-height: 550px;
	}
	.section-spacer {
    	margin: 60px 0;
	}
	.not-found {
		display: none;
		visibility: hidden;
	}
	.office-box {
    	width: 50%;
    	float: left;
	}
	.ast-single-post .entry-title, .page-title, h1, .entry-content h1 {
		font-size: 40px;
	}
	.right-one-third-office {
		margin-bottom: 20px;
		width: 100%;
		max-width: 100%;
	}
	.grid-employee-owner {
		display: block;
	}
	.employee-owner {
		padding: 0;
		margin: 20px 0 45px 0;
		width: 95%;
	}
	.project-matters-quote {
		border-left: none;
		padding: 0;
		width: 49vw;
	}
	.project-matters {
		padding: 0;
	}
	.similar-projects {
		max-height: 380px;
		max-width: 770px;
		width: 100%;
		height: calc(.493 * 100vw);
		margin-bottom: 40px;
		display: block;
		position: relative;
		float: none;
		display: block;
		position: relative;
	}
	.owner-header {
		font-size: 55px;
	}
	.employee-owner {
		max-width: 100%;
		padding: 0;
		margin: 20px 0 45px 0;
	}
	.icons-section {
		width: 40%;	
	}
	.featured-project-caption {
		max-width: 90vw;
	}
	.featured-project-caption::before {
		width: 35px;
		height: 6px;
		margin: 15px 0 4vw 0;
	}
	.project-image {
		height: calc(1.5 * 94vw);
		margin-bottom: 0;
	}
	.left-third, .middle-third, .right-third {
		display: block !important;
	    max-width: 100%;
	    width: 100%;
	}
    .our-history-grid {
        margin: 0 auto;
        padding: 0 10px;
    }
	.history-year, .description {
	    display: block !important;
	    max-width: 100%;
	    width: 100%;
	}
	.giant-details {
    	margin: 0 !important;
    	padding: 0 !important;
		float: left !important;
		display: block !important;
	}
	.giant-sidebar {
		float: none !important;
		display: block !important;
		margin: 0 auto !important;
		padding: 0 !important;
		text-align: center;
	}
	.project-details, .project-category-details {
		margin: 0;
		padding: 0 !important;
		width: 100%;
		display: block;
	}
	.project-details-details {
		margin: 70px 0 0 0;
		padding: 0 !important;
		width: 100%;
		display: block;
		min-height: 500px;
	}
	.project-details-sidebar {
		margin: 70px 0 0 0;
		padding: 0;
	}
	.white-leadership-name {
		font-size: 40px;
		color: #fff !important;
		margin: -55px 0 50px 0 !important;
		font-weight: 800;
		background-color: transparent !important;
		line-height: 1;
	}
	.leadership-details, .leadership-sidebar {
    	width: 100%;
    	margin: 0;
		padding: 0 !important;
		height: auto;
		display: block;
	}
}
@media screen and (max-width: 750px) {
	.project-details-sidebar {
		margin: 0;
		padding: 0;
	}
	.project-matters-quote {
		border-left: none;
		padding: 0;
		margin: 0 0 40px 0;
		width: 80%;
	}
	.news-item-detail-white-sidebar {
		margin: 0;
		display: inline-block;
		padding: 0 0 40px 0;
		background-color: #fff;
	}
	.news-item-detail-white-details {
		padding: 0;
	}
	a.projects-title {
		margin: 0 0 0 10px;
	}
	.employee-owner-spotlight-excerpt {
		text-align: left;
		padding: 0;
	}
	.news-item-details-sidebar {
		width: 100%;
		margin: 20px 0 50px 0;
	}
	.grid-wrapper-right-two-thirds {
		display: block;
	}
	.grid-wrapper-eo-left-two-thirds {
		display: block;
	}
	a.news-title {
		color: #000;
		background-color: transparent;
		font-weight: 700;
		line-height: 1;
		margin: 35px 0;
		display: block;
	}
	.project-card {
		width: 340px;
		z-index: 100;
	}
	.grid-employee-owner {
		display: block;
		height: 1500px;
	}
	.grid-wrapper {
		display: block;
	}
   .left-half, .right-half {
        margin: 0;
    }
	.grid-wrapper-equal-thirds {
		display: block;
	}
  	.left-third, .middle-third {
        margin: 0px;
    }
	.grid-wrapper-left-two-thirds, .grid-wrapper-left-two-thirds  {
		display: block;
    }
	.right-third-office {
		width: 100%;
		max-width: 100%;
		min-width: 310px;
	}
	.grid-wrapper-equal-fourths {
		display: block;
	}
	.left-two-thirds, .right-one-third {
	    margin-bottom: 60px;
	}
	.leadership-details {
		width: 100%;
		margin: 30px 0 0 0;
		padding: 0 !important;
	}
}
@media screen and (max-width: 720px) {
	.employee-owner {
		margin: 0 auto;
		display: block;
		width: 100%;
		padding-bottom: 50px;
	}
	.project-category-container {
		height: auto;
		width: 100%;
		max-width: unset;
		display: block;
		float: none;
		padding: 20px 0;
		position: relative;
		overflow: auto;
	}
	.project-category-photo {
		z-index: 5;
		width: 100%;
		height: auto;
		max-width: unset;
	}
}
@media screen and (max-width: 600px) {
	.service-featured-project-container {
		height: auto;
		width: 100%;
		float: none;
		padding: 0;
		margin: 0 auto;
	}
	.owner-header {
		font-size: 55px;
	}
	.news-item-black-title {
		font-size: 36px;
		font-weight: 700;
		line-height: 1;
	}
	.dropdown-menu, .form-group {
		position: relative !important;
		transform: unset !important;
		top: -42px !important;
		margin-left: -9px !important;
		will-change: unset !important;
		line-height: 1.2 !important;
	}
	primary, .dropdown, .form-group {
		color: #ffffff;
		background-color: #689BC5 !important;
		border: none;
		font-family: Raleway, sans-serif;
		font-size: 28px;
		border-radius: 0;
		width: 100%;
		height: 80px;
		margin-bottom: 70px;
		padding: 0 0 0 8px !important;
	}
	.dropdown-item {
		font-family: Raleway;
		color: #fff;
		font-size: 1.5rem;
		width: 100% !important;
		/* display: block; */
		/* position: relative; */
		line-height: 1.2 !important;
		/* padding: 8px 0 5px 10px !important; */
		height: auto;
	}
	.btn {
		font-size: 1.5rem;
	}
	.employee-owner-details, .second-half, .giant-sidebar, .giant-details, .bios-sidebar, .employee-owner-sidebar, .giant-sidebar, .leadership-sidebar  {
		height: auto;
		width: 100% !important;
		display: block;
		padding: 0 !important;
		float: none;
		margin: 0 auto;
	}
	.project-details, .project-category-details {
		margin: 60px 0 0 0;
		width: 100%;
		padding: 20px 0 0 0 !important;
	}
	.project-sidebar, .project-category-sidebar {
		height: auto;
		width: 100% !important;
		display: block;
		padding: 15px !important;
		float: none;
		margin: 50px 0 0 0;	
	}
	.similar-project-card {
		background-color: #fff;
		max-height: 130px;
		max-width: unset;
		width: 100%;
		padding: 3vw;
		top: 44vw;
		min-height: unset;
	}
	.similar-project-card-title {
		max-width: 85%;
	}
	.featured-project-caption {font-size: 14px; max-width:95%;}
	.office-grid-wrapper {
		display: grid;
		grid-template-columns: repeat(2, 1fr);
		grid-template-rows: repeat(9, 1fr);
		column-gap: 25px;
		row-gap: 40px;
		order: 1;
	}
	.always-looking {
		float: none;
		font-size: 36px;
		font-weight: bold;
		line-height: 1.2;
		position: relative;
		display: block;
	}
	.similar-projects {
		margin-bottom: 100px;
	}
	.ast-single-post .entry-title, .page-title, h1, .entry-content h1 {
		font-size: 36px;
	}
}
@media screen and (max-width: 544px) {
	.blue-quotes {padding: 0;}
	.ast-separate-container #content .ast-container {
	    padding: 0 15px;
	}
	.thin-highlight {
		font-family: Raleway;
		font-weight: 400;
		font-size: 22px;
		text-align: center;
		margin: 0 auto;
		padding: 50px 0 80px;	
	}
	.one-half, just-one-half {
		width: 100% !important;
	}
	.social-icon {
		 margin-right: 35px;
	}
	.social-icons {
		float: left;
		position: relative;
		display:inline-block;
		width: 100%;
		margin: 20px 0 0 0; 
		padding: 0 20px 30px 20px;
	}
}
@media screen and (max-width: 510px) {
	.big-green-project-button {
		margin: 10px 0 0 0;
	}
	.office-box {
    	width: 100%;
    	float: left;
	}
	.icons-section {
    	display: block !important;
		width: 100% !important;
		height: auto;
	}
	.icons {
    	width: 70% !important;
    	margin: 0 auto !important;
    	text-align: center !important;
    	height: auto;
    	padding: 0 !important;
    	display: block;
	}
	.employee-owners .pt-cv-title {
    	margin: 10px 0 !important;
	}
}	
@media screen and (max-width: 440px) {
	.projects-title {
		font-size: 15px !important;
	}
	.local-links-container {
		height: 310px;
		width: 100%;
		position: relative;
		margin: 0 0 50px 0;
		float: none;
		display: block;
		font-size: 18px;
	}
	.news-i	.big-green-project-button {
		margin: 65px 0 150px 20% !important;
		padding: 15px 0 0 0;
		width: 100%;
		font-size: 20px;
	}
	.office-grid-wrapper {
		display: block;
	}
	.featured-project {margin-bottom: 60px;}
  	.text-wrapper {width: 50%;}
	.grid-wrapper {
		grid-template-areas: "left-half right-half";
		grid-template-columns: 100%;
		grid-template-rows: auto auto;
		grid-gap: 0;
	}
  	.left-half, .right-half {
    	margin: 0;
    	border: 0;
  	}
  	.social-icons {border: 0;
		margin: 0 auto !important;
		padding: 0 !important;
  	}
  	.social-icon {
  		margin-right: 40px;
  	}
	.project-card {
		height: 125px;
		min-height: unset;
		width: 270px;
		min-width: unset;
	}
	.card-container {
		height: 125px;
		min-height: unset;
		width: 330px;
		min-width: unset;
		margin: 0 0 15px 0;
		float: right;
	}
	.project-card-orange-border {
		height: 125px;
		min-height: unset;
		margin-left: 28px;
	}
}
@media screen and (max-width: 428px) {
	.featured-project {margin-bottom: 60px;}
	.ast-button-wrap .menu-toggle.main-header-menu-toggle {
		padding: 0; 
		width: auto;
		text-align: center;
	}
	.white-leadership-name {
		font-size: 29px;
		margin: -40px 0 0 0 !important;
	}
}
</style>
</head>
<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
<?php astra_body_top(); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>
<div class="right-line"></div>
	<?php astra_header_before(); ?>
	<?php astra_header(); ?>
	<?php astra_header_after(); ?>
	<?php astra_content_before(); ?>
	<div id="content" class="site-content">
	<div class="ast-container">
	<?php astra_content_top(); ?>