<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

?>
<?php astra_content_bottom(); ?>

</div><!-- ast-container -->

</div><!-- #content -->

<?php astra_content_after(); ?>

<?php astra_footer_before(); ?>

<?php astra_footer(); ?>

<?php astra_footer_after(); ?>

</div><!-- #page -->

<?php astra_body_bottom(); ?>

<?php wp_footer(); ?>
<script>

			function theToggleFunction(j) {
		  	//alert('j');
			var x = document.getElementById('i1');
			var y = document.getElementById('i2');
			var z = document.getElementById('i3');
			var l = document.getElementById('O1');
			var m = document.getElementById('O2');
			var n = document.getElementById('O3');
			var a = document.getElementById('G1');
			var b = document.getElementById('G2');
			var c = document.getElementById('G3');  
			//alert('x'); 
		    if (j === 'i1') {
		       x.style.display = "block";
		       y.style.display = "none";
		       z.style.display = "none";
		       l.style.backgroundColor= '#F9A134'; 
		       m.style.backgroundColor= 'transparent'; 
		       n.style.backgroundColor= 'transparent'; 
		       a.style.display = "block";
		       b.style.display = "none";
		       c.style.display = "none";
		    }
		    if (j === 'i2') {
		       x.style.display = "none";
		       y.style.display = "block";
		       z.style.display = "none";
		       l.style.backgroundColor= 'transparent'; 
		       m.style.backgroundColor= '#F9A134'; 
		       n.style.backgroundColor= 'transparent';
		       a.style.display = "none";
		       b.style.display = "block";
		       c.style.display = "none";
		    }
		    if (j === 'i3') {
		       x.style.display = "none";
		       y.style.display = "none";
		       z.style.display = "block";
		       l.style.backgroundColor= 'transparent'; 
		       m.style.backgroundColor= 'transparent'; 
		       n.style.backgroundColor= '#F9A134'; 
		       a.style.display = "none";
		       b.style.display = "none";
		       c.style.display = "block";
			}
		}
		</script>
</div>
	</body>
</html>
