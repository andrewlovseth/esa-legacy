<?php

/*
	Enqueue Styles & Scripts
*/


// Enqueue custom styles and scripts
function esa_enqueue_styles_and_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/esa.css', '', '');
}
add_action( 'wp_enqueue_scripts', 'esa_enqueue_styles_and_scripts' );