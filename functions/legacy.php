<?php 

/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );
/**
 * Enqueue styles
 */
function child_enqueue_styles() {
	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 ); 

/**
 * Change the breakpoint of the Astra Header Menus
 * 
 * @return int Screen width when the header should change to the mobile header.
 */
function your_prefix_change_header_breakpoint() {
	return 1200;
};
 
add_filter( 'astra_header_break_point', 'your_prefix_change_header_breakpoint' );



/** Disable comments on media files ***/
function add_filter_media_comment ( $open, $post_id ) {
 $post = get_post( $post_id );
 if( $post->post_type == 'attachment' ) {
 return false;
 }
 return $open;
}
add_filter( 'comments_open', 'add_filter_media_comment', 10 , 2 );



function my_login_logo_one() { 
?> 
<style type="text/css">
body {
	background-color: #fff !important;
	color: #333;
}
body.login div#login h1 a {
    background-image: url(https://esassoc.com/wp-content/uploads/2019/05/ESA-Logo.svg);
    height: 120px;
    width: 320px;
    padding: 0 0 15px 0;
    background-size: 275px;
    background-color: #fff;
    margin: 0 auto !important;
} 
.login form {
	margin-top: 20px;
	margin-left: 0;
	padding: 26px 24px 46px;
	background: #fff;
	-webkit-box-shadow: 0 1px 3px rgba(0,0,0,.13);
	box-shadow: 2px 2px 2px #0006;
	border: 1px solid #0006;
	border-radius: 8px;
}
</style>
 <?php 

} add_action( 'login_enqueue_scripts', 'my_login_logo_one' );


// Disable Post featured image.
add_filter( 'astra_featured_image_enabled', '__return_false' );


// Disable wpautop 
remove_filter ('the_content', 'wpautop');
remove_filter ('the_excerpt', 'wpautop');

/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s| )+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);


function title_length($title='', $type, $object) {
    $return = 0;
 if ( !empty( $title ) ) {
            $return = strlen($title);
    } 
    return $return;
}

add_filter( 'wp_default_editor', create_function('', 'return "html";') ); 

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
	.wpt-form-description {
		font-size: 0.9em;
		color: #CA4A1F !important;
		margin-bottom: 7px;
		font-weight: 600 !important;
	} 
	#adminmenu li.wp-menu-separator {
		height: 5px;
		padding: 0;
		margin: 0 0 6px 0;
		cursor: inherit;
		border-bottom: 2px solid lightblue;
	}
  </style>';
}

add_action('after_setup_theme','remove_core_updates');
function remove_core_updates()
{
 if(! current_user_can('update_core')){return;}
 add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
 add_filter('pre_option_update_core','__return_null');
 add_filter('pre_site_transient_update_core','__return_null');
}

